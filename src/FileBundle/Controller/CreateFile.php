<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 06.06.18
 * Time: 17:25
 */

namespace App\FileBundle\Controller;

use App\FileBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;


final class CreateFile
{

    public function __invoke(Request $request, KernelInterface $kernel, EntityManagerInterface $em): ?File
    {
        $dir = $kernel->getRootDir(). '/../public/uploads/';
        $currentDate = new \DateTime();
        $currentDate = $currentDate->format('Y-m-d');

        $dir .= $currentDate;

        if(!file_exists($dir))
            mkdir($dir, 0777, true);


        /** @var UploadedFile $file */
        foreach ($request->files as $file) {
            $name = md5($this->generateRandomString());
            $ext = $file->getClientOriginalExtension();

            $file->move($dir, $name.'.'.$ext);

            /** @var File $objFile */
            $objFile = new File();
            $objFile->name = $name;
            $objFile->path = $currentDate.'/'.$name.'.'.$ext;

            $em->persist($objFile);
            $em->flush();
        }

        return $objFile;
    }

    private function generateRandomString($length = 50)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}