<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 22.04.2018
 * Time: 21:57
 */

namespace App\FileBundle\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use App\FileBundle\Controller\CreateFile;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 *     "post"={
 *         "method"="POST",
 *         "path"="/files",
 *         "controller"=CreateFile::class,
 *         "defaults"={"_api_receive"=false},
 *         "swagger_context"={
 *              "responses"={
 *                  "201"={"description"="ok"},
 *                  "404"={"description"="Not Found"},
 *                  "400"={"description"="Bad Request"},
 *                  "409"={"description"="data is used"},
 *              },
 *              "parameters"={
 *                  {
 *                      "name"="file",
 *                      "in"="formData",
 *                      "type"="file"
 *                  },
 *              },
 *           },
 *        },
 *     },
 *     attributes={
 *       "normalization_context"={"groups"={"GetFile", "GetObjBase"}},
 *       "denormalization_context"={"groups"={"SetFile"}}
 *     }
 * )
 *
 */
class File extends BaseEntity
{
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     * @Groups({"GetFile"})
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     * @Groups({"GetFile"})
     */
    public $path;
}