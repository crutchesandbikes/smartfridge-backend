<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 2:01
 */

namespace App\ProductBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={ "GetBase","GetProduct","ProductCategory","GetObjCategory"}},
 *     denormalizationContext={"groups"={"SetProduct","SetCategory"}},
 * )
 * @UniqueEntity({"code"})
 */
class Good extends BaseEntity
{

    /**
     * @var
     * @ORM\Column(type="string", name="code",nullable=true)
     * @Groups({"SetProduct", "GetProduct", "GetObjProduct"})
     */
    public $code;


    /**
     * @var
     * @ORM\Column(type="integer", name="life",nullable=true)
     * @Groups({"SetProduct", "GetProduct", "GetObjProduct"})
     */
    public $life;
    /**
     * @var
     * @ORM\Column(type="string", name="name")
     * @Assert\NotBlank()
     * @Groups({"SetProduct", "GetProduct", "GetObjProduct"})
     */
    public $name;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"SetProduct", "ProductCategory"})
     */
    public $category;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\FileBundle\Entity\File")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"SetProduct", "GetProduct"})
     */
    public $file;


}