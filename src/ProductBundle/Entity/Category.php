<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 2:32
 */

namespace App\ProductBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={"GetCategory", "GetBase"}},
 *     denormalizationContext={"groups"={"SetCategory"}},
 * )
 */
class Category extends BaseEntity
{

    /**
     * @var
     * @ORM\Column(type="string", name="name")
     * @Assert\NotBlank()
     * @Groups({"SetCategory", "GetCategory", "GetObjCategory"})
     */
    public $name;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\FileBundle\Entity\File")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"SetCategory", "GetCategory"})
     */
    public $file;
}