<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 4:08
 */

namespace App\ProductBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\ProductBundle\Controller\CreateUserProduct;
/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "method"="POST",
 *              "path"="/user_goods",
 *              "controller"=CreateUserProduct::class
 *          },
 *     },
 *     normalizationContext={"groups"={"GetUserGood", "GetBase",
 *                                     "UserProductProduct",
 *                                     "GetObjProduct"
 *
 *     }},
 *     denormalizationContext={"groups"={"SetUserGood"}},
 * )
 */
class UserGood extends BaseEntity
{

    /**
     * @var 
     * @ORM\ManyToOne(targetEntity="Good")
     * @Groups({"SetUserGood", "UserProductProduct"})
     */
    public $good;
    
    /**
     * @var 
     * @ORM\ManyToOne(targetEntity="App\BaseBundle\Entity\User")
     */
    public $user;
    
    
    /**
     * @var 
     * @ORM\Column(type="float", name="count")
     * @Groups({"SetUserGood", "GetUserGood", "GetObjUserGood"})
     */
    public $count;

    /**
     * @var 
     * @ORM\Column(type="integer", name="lift")
     * @Groups({"SetUserGood", "GetUserGood", "GetObjUserGood"})
     */
    public $life;

}