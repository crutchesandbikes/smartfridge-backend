<?php
/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 14.05.18
 * Time: 18:34
 */

namespace App\BaseBundle\Controller;

use App\BaseBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreateUser
{
    private $security;
    private $em;
    public function __construct(Security $security,EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->security = $security;
    }


    /**
     * @param User $data
     * @return User
     * @throws \Exception
     */
    public function __invoke(User $data): User
    {
        $em = $this->em;
        $user = $em->getRepository(User::class)->findOneBy(["username"=>$data->getUsername()]);
        if (isset($user)){
            throw new HttpException(400,'username already used ');
        }
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            $data->setRolesRaw(['ROLE_USER']);
            $em->flush();

        } else {
            $data->setRolesRaw($data->getRoles());
        }
        $data->setEnabled(true);

        $data->setPlainPassword($data->getPassword());

        /** @var User $user */
        return $data;
    }
}