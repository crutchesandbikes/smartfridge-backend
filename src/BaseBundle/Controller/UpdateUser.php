<?php
/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 14.05.18
 * Time: 18:34
 */

namespace App\BaseBundle\Controller;


use App\BaseBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UpdateUser
{
    private $security;
    private $em;
    private $encoder;

    public function __construct(Security $security, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->security = $security;
        $this->encoder = $encoder;
    }

    public function __invoke(User $data): User
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if (!is_object($user))
            throw new HttpException(401, "Not Authorized");
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            if ($data->getId() != $user->getId()) {
                throw new HttpException(403, "Forbidden");
            }
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            $data->setRolesRaw(['ROLE_USER']);
        } else {
            $data->setRolesRaw($data->getRoles());
        }

        /** @var User $user */
        return $data;
    }
}