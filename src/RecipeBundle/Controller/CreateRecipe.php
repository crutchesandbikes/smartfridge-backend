<?php
/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 14.05.18
 * Time: 18:34
 */

namespace App\RecipeBundle\Controller;

use App\RecipeBundle\Entity\Recipe;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreateRecipe
{
    private $security;
    private $em;
    public function __construct(Security $security,EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->security = $security;
    }



    public function __invoke(Recipe $data): Recipe
    {
        $user = $this->security->getUser();
        $data->user = $user;

        return $data;
    }
}