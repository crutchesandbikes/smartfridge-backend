<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 2:26
 */

namespace App\RecipeBundle\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use App\ProductBundle\Entity\Good;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\RecipeBundle\Controller\CreateRecipe;
use App\Filter\MultySearchFilter;
use App\Filter\AndSearhFilter;

//"SetRecipeProduct","RecipeStep","SetStep","ProductStepProduct","SetStepProduct","StepProductProduct","SetProduct"
/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "method"="POST",
 *              "path"="/recipes",
 *              "controller"=CreateRecipe::class
 *          },
 *     },
 *     normalizationContext={"groups"={"GetBase","GetRecipe","GetRecipeGood",
     *                                               "RecipeProduct","RecipeStep","ProductStepProduct","StepProductProduct",
 *                                               "GetObjProduct","GetRecipeProduct","GetStep","GetStepProduct",
 *     }},
 *     denormalizationContext={"groups"={"SetRecipe","SetRecipeGood","SetStepGood","SetStepProduct","SetStepProduct","SetStep"}},
 * )
 * @ApiFilter(MultySearchFilter::class,properties={"products.id"})
 * @ApiFilter(AndSearhFilter::class,properties={"products.id"})
 */
class Recipe extends BaseEntity
{
    /**
     * @var
     * @ORM\Column(type="string", name="name")
     * @Assert\NotBlank()
     * @Groups({"SetRecipe", "GetRecipe", "GetObjRecipe"})
     */
    public $name;

    /**
     * @var
     * @ORM\Column(type="string", name="description",nullable=true)
     * @Groups({"SetRecipe", "GetRecipe", "GetObjRecipe","SetRecipeGood"})
     */
    public $description;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="RecipeGood",mappedBy="recipe",cascade={"persist"})
     * @Groups({"SetRecipe", "GetRecipe"})
     */
    public $goods;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\BaseBundle\Entity\User")
     * @Groups({"GetRecipe"})
     */
    public $user;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\FileBundle\Entity\File")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"SetRecipe", "RecipeProduct"})
     */
    public $file;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="Step", mappedBy="recipe",cascade={"persist"})
     * @Groups({"SetRecipe","RecipeStep"})
     */
    public $steps;


    public function addGood(RecipeGood $good): void
    {
        $good->recipe = $this;
        $this->goods->add($good);
    }

    public function removeGood(RecipeGood $good): void
    {
        $good->recipe = null;
        $this->goods->removeElement($good);
    }

    public function addStep(Step $step): void
    {
        $this->steps->add($step);
    }

    public function removeStep(Step $step): void
    {
        $this->steps->removeElement($step);
    }

    public function __construct()
    {
        parent::__construct();
        $this->steps = new ArrayCollection();
        $this->goods = new ArrayCollection();
    }

}