<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 11:18
 */

namespace App\RecipeBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={"GetRecipeGood", "GetBase"}},
 *     denormalizationContext={"groups"={"SetRecipeGood"}},
 * )
 */
class RecipeGood extends BaseEntity
{

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Recipe", inversedBy="goods")
     */
    public $recipe;

    /**
     * @var
     * @ORM\Column(type="float", name="count",nullable=true)
     * @Groups({"SetRecipeGood", "GetRecipeGood", "GetObjRecipeGood"})
     */
    public $count;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Good")
     * @Groups({"SetRecipeGood", "GetRecipeGood"})
     */
    public $good;
}