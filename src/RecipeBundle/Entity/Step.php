<?php

/**
 * Created by PhpStorm.
 * User: zawert
 * Date: 08.12.18
 * Time: 3:41
 */

namespace App\RecipeBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\BaseBundle\Entity\BaseEntity;
use App\ProductBundle\Entity\Good;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={"GetStep","GoodStepGood","GetObjGood", "GetBase"}},
 *     denormalizationContext={"groups"={"SetStep","GoodStepGood"}},
 * )
 */
class Step extends BaseEntity
{

    /**
     * @var 
     * @ORM\ManyToOne(targetEntity="Recipe", inversedBy="steps")
     * @Groups({"StepRecipe"})
     */
    public $recipe;
    
    /**
     * @var
     * @ORM\Column(type="string", name="name")
     * @Assert\NotBlank()
     * @Groups({"SetStep", "GetStep", "GetObjStep"})
     */
    public $name;

    /**
     * @var
     * @ORM\Column(type="text", name="message")
     * @Assert\NotBlank()
     * @Groups({"SetStep", "GetStep", "GetObjStep"})
     */
    public $message;


    /**
     * @var
     * @ORM\Column(type="integer", name="time",nullable=true)
     * @Groups({"SetStep", "GetStep", "GetObjStep"})
     */
    public $time;

    public function addGood(Good $good): void
    {
        $this->goods->add($good);
    }

    public function removeGood(Good $good): void
    {
        $this->goods->removeElement($good);
    }


    public function __construct()
    {
        parent::__construct();
        $this->goods = new ArrayCollection();
    }

}