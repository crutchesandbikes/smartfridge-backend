<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10.07.18
 * Time: 17:12
 */


namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

final class MultySearchFilter extends AbstractContextAwareFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if($property != 'multysearch') return;

        $key    = array_keys($value)[0];
        $values = explode(",", $value[$key]);
        $keys   = explode(".", $key);

        if(!isset($keys[0]) || !isset($keys[1]))
        return;

        $queryBuilder
            ->leftJoin("o.$keys[0]",$keys[0])
            ->andWhere($keys[0].".$keys[1] IN (:valueMultySearch)")
            ->setParameter('valueMultySearch', $values);
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {

        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {

            $description["multysearch_$property"] = [
                'property' => "multysearch[".$property."]",
                'type' => 'array[string]',
                'required' => false,
                'swagger' => [
                    'description' => 'Filter for search with multy values ',
                    'name' => "multysearch[$property]",
                    'type' => 'array[string]',
                ],
            ];
        }

        return $description;
    }
}