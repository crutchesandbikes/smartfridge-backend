<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10.07.18
 * Time: 17:12
 */


namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

final class AndSearhFilter extends AbstractContextAwareFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if($property != 'andSearch') return;

        $key    = array_keys($value)[0];
        $values = explode(",", $value[$key]);
        $keys   = explode(".", $key);

        if(!isset($keys[0]) || !isset($keys[1]))
        return;
        $queryBuilder
            ->leftJoin("o.$keys[0]",$keys[0]);
        foreach ($values as $key =>$value) {
            $queryBuilder->andWhere($keys[0].".$keys[1] = :value$key")
                ->setParameter("value$key", $value);
        }

    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {

        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {

            $description["andSearch_$property"] = [
                'property' => "andSearch[$property]",
                'type' => 'array[string]',
                'required' => false,
                'swagger' => [
                    'description' => 'Filter for search with multy values ',
                    'name' => "andSearch[$property]",
                    'type' => 'array[string]',
                ],
            ];
        }

        return $description;
    }
}